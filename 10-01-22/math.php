<?php
    $p = pi();
    // $p = 'python';
    echo $p.'<br/>';
    echo sqrt(64).'<br/>';
    echo min(4,43,55,-33).'<br/>';
    echo max(4,43,55,-33).'<br/>';
    echo pow(2,4).'<br/>';
    echo rand().'<br/>';
    echo abs(-34).'<br/>';
    echo round(23.1).'<br/>';
    echo is_float(23.1).'<br/>';
    echo is_int(23).'<br/>';

    // constants
    define('name', 'Peter Parker', true);
    // name = 'james'; // // We can't update constants's value
    echo name.'<br/>';
    echo NaMe.'<br/>';
?>