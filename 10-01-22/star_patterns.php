<!DOCTYPE html>
<html lang="en">
<head>
    <title>Star Patterns</title>
</head>
<body>
    <?php
        for($i=1; $i<=5; $i++){
            for($j=1; $j<=$i; $j++){
                echo '* ';
            }
            echo '<br>';
        }
    ?> <br>
    <?php
        for($i=5; $i>=1; $i--){
            for($j=1; $j<=$i; $j++){
                echo '* ';
            }
            echo '<br>';
        }
    ?> <br>

    <?php
        for($row=1; $row<=10; $row++){
            for($column=1; $column<=$row; $column++){

                if($column==1||$column==$row||$row==10){
                    echo '*';
                }else{
                    echo '&nbsp; ';   
                }
                
            }
            echo '<br>';
        }
    ?>
</body>
</html>