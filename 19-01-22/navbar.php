
<nav class="navbar bg-dark navbar-dark navbar-expand fixed-top">
    <a href="" class="navbar-brand">MyWeb</a>
    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <a href="../17-01-22/register_form.php" class="nav-link">Home</a>
        </li>
        <li class="nav-item">
            <a href="../19-01-22/all_users.php" class="nav-link">All Users</a>
        </li>
        <?php if(isset($_SESSION["user_email"])){ ?>
        <li class="nav-item">
            <a href="../27-01-22/dashboard.php" class="nav-link">Dashboard</a>
        </li>
        <li class="nav-item">
            <a href="../28-01-22/rest_api.php" class="nav-link">Countries</a>
        </li>
        <li class="nav-item">
            <a href="../25-01-22/logout.php" class="nav-link">Logout</a>
        </li>
        <?php } ?>
    </ul>
</nav>
