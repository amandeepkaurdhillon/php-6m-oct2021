<!-- CRUD 
C - Create 
R - Read / Retrieve
U - Update
D - Delete -->
<?php

session_start();
    include('../17-01-22/db-connection.php');
    
    $q = "SELECT * FROM register ORDER BY -id";
    // $q = "SELECT * FROM register WHERE id=1";
    // $q = "SELECT * FROM register WHERE gender='m'";
    // $q = "SELECT * FROM register WHERE name='test'";
    // $q = "SELECT * FROM register WHERE status=1";
    $result = $con->query($q);

    $no_of_users = $result->num_rows;

    // // Display data
    // while($row = $result->fetch_assoc()){
    //     echo 'id:'.$row['id'].', name: '.$row['name'].'<br/>';
    // }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Document</title>
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <script src="../jquery.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="../fontawesome/css/all.min.css">
    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <script src="../bootstrap/js/bootstrap.min.js"></script>
    <style>
        .bg-image{
            background:url('../images/professional.jpg');
            background-size: cover;
            height: 250px;
            color: white;
            background-attachment: fixed;
        }
        .bg-image h1{line-height: 250px;}
        td button{border:none};
    </style>
</head>
<body>
    <?php 
    include('navbar.php'); 

    if(!(isset($_SESSION["user_email"]))){
        die("<h1 class='alert alert-danger mt-5'>Please Login First to view this page</h1>");
        session_destroy();
    }
    
    ?>
  <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 bg-image">
                <h1 class="text-uppercase text-center">All Users (<?php echo $no_of_users; ?>)</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 mt-5">
                <table class="table table-striped table-hover" id="all_users">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Gender</th>
                            <th>Status</th>
                            <th>Member Since</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        while($row = $result->fetch_assoc()){
                            echo '<tr>';
                            echo '<td>'.$row['id'].'</td>';
                            echo '<td>'.$row['name'].'</td>';
                            echo '<td>'.$row['email'].'</td>';
                            echo '<td>';
                            if($row['gender']=="m"){
                                echo 'male';
                            }else if($row['gender']=="f"){
                                echo 'female';
                            }else if($row['gender']=="o"){
                                echo 'others';   
                            }
                            echo '</td>';
                            echo '<td>'.$row['registered_on'].'</td>';
                            echo '<td>';
                            if($row['status']==1){
                                echo '<button class="btn btn-success btn-sm">Active</button>';
                            }else{
                                echo '<button class="btn btn-danger btn-sm">Disabled</button>';
                            }
                            echo '</td>';
                            echo '<td>';
                            echo '<button data-toggle="modal" data-target="#myModal'.$row['id'].'" class="bg-info text-white p-1 mr-1"><i class="fas fa-edit"></i></button>';
                            echo '<button data-toggle="modal" data-target="#myDeleteModal-'.$row['id'].'" class="bg-danger text-white p-1"><i class="fas fa-trash"></i></button>';
                            echo '</td>';
                            echo '</tr>';

                            echo '<div class="modal fade" id="myModal'.$row['id'].'">';
                                echo '<div class="modal-dialog">';
                                    echo '<div class="modal-content">';
                                        echo '<div class="modal-header">';
                                            echo '<h4 class="text-center">Update User</h4>';
                                            echo '<button class="close" data-dismiss="modal">&times;</button>';
                                        echo '</div>';
                                        echo '<div class="modal-body">';
                                            echo '<form method="post" action="../21-01-22/update_user.php">';
                                            echo '<input type="hidden" name="id" class="form-control" value="'.$row['id'].'">';
                                            echo '<input type="text" name="name" placeholder="Full Name" class="form-control" value="'.$row['name'].'">';
                                            echo '<input type="text" name="email" placeholder="Email Address" class="form-control mt-3" value="'.$row['email'].'">';
                                            echo '<input type="text" name="password" placeholder="Set New Password" class="form-control mt-3">';
                                            echo '<select name="gender" class="form-control mt-3">';
                                                echo '<option value="m" >Male</option>';
                                                echo '<option value="f">Female</option>';
                                                echo '<option value="o">Others </option>';
                                            echo '</select>';
                                            echo '<input type="submit" name="update_user" value="Save Changes" class="btn btn-info mt-3 btn-block">';
                                            echo '</form>';
                                        echo '</div>';
                                    echo '</div>';
                                echo '</div>';
                            echo '</div>';

                            echo '<div class="modal fade" id="myDeleteModal-'.$row['id'].'">';
                                echo '<div class="modal-dialog">';
                                    echo '<div class="modal-content">';
                                        echo '<div class="modal-header">';
                                            echo '<h4 class="text-center">Delete Confirmation</h4>';
                                            echo '<button class="close" data-dismiss="modal">&times;</button>';
                                        echo '</div>';
                                        echo '<div class="modal-body">';
                                            echo '<form method="post" action="../21-01-22/delete_user.php">';
                                            echo '<label>Are you sure want to delete '.$row['name'].'?</label>';
                                            echo '<input type="hidden" name="id" class="form-control" value="'.$row['id'].'"><br><br>';
                                            echo '<input type="submit" value="Yes Delete" class="btn btn-danger my-3 mx-1">';
                                            echo '<button class="btn btn-secondary my-3 mx-1" data-dismiss="modal">No</button>';
                                            echo '</form>';
                                        echo '</div>';
                                    echo '</div>';
                                echo '</div>';
                            echo '</div>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <?php include('../27-01-22/footer.php'); ?>
    <script>
        $(function(){
            $("#all_users").DataTable();
        });
    </script>
</body>
</html>