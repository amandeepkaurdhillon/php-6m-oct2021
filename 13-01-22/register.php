<!DOCTYPE html>
<html lang="en">
<head>
    <title>Register</title>
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
</head>
<body>
<?php
if(isset($_POST['name'])){
    $name = $_POST['name'];
    $email = $_POST['email'];
    $pass = $_POST['password'];
    $cpass = $_POST['c_password'];
    $gen = $_POST['gender'];

    if($pass != $cpass){
        echo '<div class="text-center alert alert-danger m-5 p-5">';
        echo "<h4>Password didn't matched!</h4>";
        echo '<a href="form_handling.php">Back to homepage</a>';        
        echo '</div>';
    }else{
        echo '<div class="text-center alert alert-success m-5 p-5">';
        echo '<h1>Welcome '.$name.'!!!</h1>';
        echo '<p>Email: '.$email.'</p>';
        echo '<p>Gender: '.$gen.'</p>';
        echo '<p>Password: '.$pass.'</p>';
        echo '<a href="form_handling.php">Back to homepage</a>';
        echo '</div>';
    }
}
?>
</body>
</html>