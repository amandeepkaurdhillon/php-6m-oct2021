<!DOCTYPE html>
<html lang="en">
<head>
    <title>Document</title>
</head>
<body>
<?php 
// // PHP Arrays
// Indexed Arrays
// Associative Arrays 
// Multi-dimentional Arrays
    $languages = array('PHP', 'Python', '.NET', 'Java');

    // print_r($languages);
    echo $languages[2].'<br/>';

    $length = count($languages);

    for($i = 0; $i < $length; $i++ ){
        echo $i.' = '.$languages[$i].'<br/>'; 
    }
?>    
</body>
</html>