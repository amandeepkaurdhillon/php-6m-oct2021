<?php

$colors = array('red', 'green', 'blue', 'magenta');

echo '<ol>';
foreach($colors as $v){
    echo '<li style="color:'.$v.'">'.$v.'</li>';
}
echo '</ol>';

$student = array(
    'name' => 'Peter',
    'age' => 22,
    'present' => true,
);

foreach($student as $k=>$v){
    echo $k.' : '.$v.'<br/>';
}
?>