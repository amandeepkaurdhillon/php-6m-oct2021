<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <title>Login Page</title>
</head>
<body>
    <?php include('../19-01-22/navbar.php'); ?>
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-6 mx-auto bg-info text-light my-5 p-4">
                <h4 class="text-center">Login Here</h4>
                <form action="login.php" method="post">
                    <div class="form-group">
                        <label>Email Address</label>
                        <input type="email" class="form-control" name="email" placeholder="Email Address" required>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" name="password" placeholder="Password" required>
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Log In" class="btn btn-default" name="login">
                    </div>
                    <div class="form-group">
                        <label class="text-light">Don't have an account?</label>
                         <a href="../17-01-22/register_form.php" class="text-light">Register Here</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php include('../27-01-22/footer.php'); ?>
</body>
</html>