<!DOCTYPE html>
<html lang="en">
<head>
    <title>For Loop</title>
</head>
<body>
    <?php 
        // for($i=1; $i<=10; $i++){
        //     echo $i.'<br/>';
        // }

        for($i=10; $i>=1; $i--){
            echo $i.'<br/>';
        }
    ?>
</body>
</html>

