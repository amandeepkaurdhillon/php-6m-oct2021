<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Variables</title>
</head>
<body>
    <?php
        $a = 10;
        $b = 20;

        echo "Sum: ".($a + $b);
    ?>
    <h1>Concatenation in PHP</h1>
    <?php
        $color = 'red';

        echo 'I like '.$color.' color!';
    ?>
</body>
</html>