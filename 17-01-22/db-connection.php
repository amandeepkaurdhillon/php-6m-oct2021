<?php

$host = 'localhost';
$user = 'root';
$password = '';
$db_name = 'batch2022';

// Create connection
$con = new mysqli($host, $user, $password, $db_name);

// Check connection
if($con -> connect_error){
    die('Connection failed : '.$con->connect_error);
}
// echo 'Connected successfully!';
?>