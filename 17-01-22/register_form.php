<!DOCTYPE html>
<html lang="en">
<head>
    <title>Register Page</title>
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <script src="../jquery.js"></script>
</head>
<body>
    <?php include('../19-01-22/navbar.php'); ?>
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-6 mx-auto bg-info text-light my-5 p-4">
                <h4 class="text-center">Register Here</h4>
                <form action="insert_data.php" method="post">
                    <div class="form-group">
                        <label>Full Name</label>
                        <input type="text" class="form-control" name="full_name" required>
                    </div>             
                    <div class="form-group">
                        <label>Email <small class="text-light" id="msz"></small> </label>
                        <input type="email" class="form-control" name="email" id="email" onkeyup="checkUser()" required>
                    </div>             
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" name="pass" required>
                    </div>             
                    <div class="form-group">
                        <label>Gender</label>
                        <select name="gen" class="form-control">
                            <option value="m">male</option>
                            <option value="f">female</option>
                            <option value="o">others</option>
                        </select>
                    </div>             
                    <div class="form-group">
                        <input type="submit" class="btn btn-warning" value="Click To Continue">
                    </div>
                    <div class="form-group">
                        <label class="text-light">Already have an account?</label>
                         <a href="../25-01-22/login_form.php" class="text-light">Login Here</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php include('../27-01-22/footer.php'); ?>
    <script>
        function checkUser(){
            let em = document.getElementById("email").value;
            $.ajax({
                url: "../27-01-22/user_exist_validation.php",
                type:"post",
                data:{email:em},
                success:function(data){
                    if(data==1){
                        $("#msz").html("A user with this email already exists!");
                        $("input[type='submit']").attr('disabled', true);
                    }else{
                        $("#msz").html("");
                        $("input[type='submit']").attr('disabled', false);
                    }
                }
            })
        }
    </script>
</body>
</html>