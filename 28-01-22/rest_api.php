<?php
error_reporting(0);
session_start();
$apiUrl = 'https://restcountries.com/v2/all';
$result = file_get_contents($apiUrl);
$data_to_php = json_decode($result, true);
// echo '<pre>';
// print_r($data_to_php);
// echo '</pre>';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rest Countries</title>
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
    <script src="../jquery.js"></script>
    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
</head>
<body>
    <?php include('../19-01-22/navbar.php'); ?>
    <div class="container-fluid my-5">
        <div class="row">
            <div class="col-md-12">
                <h1 class="text-center my-2">All Countries (<?php echo count($data_to_php); ?>) </h1>
                <table class="table table-striped table-hover" id="myTable">
                    <thead class="thead-dark">
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Capital</th>
                            <th>Area</th>
                            <th>Population</th>
                            <th>Flag</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            for($i=0; $i<count($data_to_php); $i++){
                                echo '<tr>';
                                echo '<td>'.$i.'</td>';
                                echo '<td>'.$data_to_php[$i]['name'].'</td>';
                                echo '<td>'.$data_to_php[$i]['capital'].'</td>';
                                echo '<td>'.$data_to_php[$i]['area'].'</td>';
                                echo '<td>'.$data_to_php[$i]['population'].'</td>';
                                echo '<td><img src="'.$data_to_php[$i]['flag'].'" style="height:100px;width:150px;"></td>';
                                echo '</tr>';
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <?php include('../27-01-22/footer.php'); ?>
    <script>
        $("#myTable").dataTable();
    </script>
</body>
</html>