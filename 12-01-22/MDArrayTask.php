<!DOCTYPE html>
<html lang="en">
<head>
    <title>PHP Array</title>
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
</head>
<body>
    <?php
    $students = array(
        array('James', 20, 'Male'),
        array('Mike', 22, 'Male'),
        array('Jenny', 21, 'Female'),
        array('Peter', 22, 'Male'),
        array('James', 20, 'Male'),
        array('Mike', 22, 'Male'),
        array('Jenny', 21, 'Female'),
        array('Peter', 22, 'Male'),
    );

    ?>
    <table border="1" class="table table-striped table-hover table-info">
        <thead class="thead-dark">
            <tr>
                <th>Sr. No</th>
                <th>Name</th>
                <th>Age</th>
                <th>Gender</th>
            </tr>
        </thead>
        <tbody>
        <?php
            $c = 1;
            foreach($students as $st){
                echo '<tr>';
                echo '<td>'.$c.'</td>';
                echo '<td>'.$st[0].'</td>';
                echo '<td>'.$st[1].'</td>';
                echo '<td>'.$st[2].'</td>';
                echo '</tr>';
                $c++;
            }
        ?>
        </tbody>
    </table>
</body>
</html>