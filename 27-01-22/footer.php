<footer>
    <div class="container-fluid bg-dark text-light pt-4">
        <div class="row">
            <div class="col-md-4">
                <h4>My Website</h4>
                <p><small>Lorem ipsum dolor sit amet consectetur adipisicing elit. Alias, quo id! Autem corporis iusto aliquid similique placeat odit esse debitis. Dolorum vitae architecto minus commodi sapiente, sunt alias necessitatibus dignissimos?</small></p>
            </div>
            <div class="col-md-4 text-light">
                <p><a href="">Page 1</a></p>
                <p><a href="">Page 2</a></p>
                <p><a href="">Page 3</a></p>
                <p><a href="">Page 4</a></p>
            </div>
            <div class="col-md-4">
                <p>#1234, Abcd Colony, Mohali</p>
                <p><em>mywebsite@myweb.com</em></p>
                <p><em>+91 9898989888</em></p>
                <p><em>162 2232323</em></p>
            </div>
            <div class="col-md-12 text-center my-2">
                <p>&copy;2022.All rights reserved.</p>
            </div>
        </div>
    </div>
</footer>