<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <style>
        .shadow{
            box-shadow: 0px 0px 15px gray;
        }
        .profile-pic{
            height:80px;width:80px;text-align:center;
            line-height:80px;background:gray;color:white;font-size:20px;
            display:inline-block;
            border-radius:50%;
        }
    </style>
    <title>Dashboard - My Website</title>
</head>
<body>

    <?php 
    session_start();
    include('../17-01-22/db-connection.php'); 
    include('../19-01-22/navbar.php'); 
    
    
    $email =  $_SESSION["user_email"];
    $q = "SELECT name,email,gender,status,registered_on FROM register WHERE email='$email'";    
    $res = $con->query($q);

    while($row = $res->fetch_assoc()){
        $name = $row['name'];
        $em = $row['email'];
        $gender = $row['gender'];
        $status = $row['status'];
        $date = $row['registered_on'];
    }
    ?>
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-6 mx-auto my-5">
                <div class="card shadow">
                    <div class="card-header">
                        <h4>My Profile</h4>
                    </div>
                    <div class="card-body">
                        <div class="text-center my-2"><span class="profile-pic"><?php echo $name[0];?></span></div>
                        <table class="table table-striped table-hover">
                            <tr>
                                <th style="width:50%;">Name</th>
                                <td><?php echo $name; ?></td>
                            </tr>
                            <tr>
                                <th style="width:50%;">Email</th>
                                <td><?php echo $em; ?></td>
                            </tr>
                            <tr>
                                <th style="width:50%;">Gender</th>
                                <td><?php
                                if($gender=="m"){
                                    echo "Male";
                                }else if($gender=="f"){
                                    echo "Female";
                                }else{
                                    echo "Others";
                                }
                                ?></td>
                            </tr>
                            <tr>
                                <th style="width:50%;">Registration Date</th>
                                <td><?php echo $date; ?></td>
                            </tr>
                            <tr>
                                <th style="width:50%;">Account status</th>
                                <td><?php if($status==1){echo "Active";}else{echo "Deactivated";} ?></td>
                            </tr>
                        </table>
                    </div>
                    <div class="card-footer">
                        <a href="../25-01-22/logout.php" class="nav-link">Logout</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include('footer.php'); ?>
</body>
</html>